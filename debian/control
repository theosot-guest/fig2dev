Source: fig2dev
Section: graphics
Priority: optional
Maintainer: Roland Rosenfeld <roland@debian.org>
Standards-Version: 4.4.1
Build-Depends: debhelper-compat (= 12),
               gawk,
               ghostscript,
               libpng-dev,
               libxpm-dev (>= 1:3.5.4.2),
               netpbm,
               texlive-font-utils,
               texlive-fonts-recommended,
               texlive-lang-german,
               texlive-latex-base,
               texlive-latex-extra,
               texlive-latex-recommended,
               texlive-pictures (>= 2013.20140314),
               xutils-dev
Homepage: https://sourceforge.net/projects/mcj/
Vcs-Git: https://salsa.debian.org/debian/fig2dev.git
Vcs-Browser: https://salsa.debian.org/debian/fig2dev
Rules-Requires-Root: no

Package: fig2dev
Architecture: any
Depends: gawk,
         ghostscript,
         netpbm (>= 2:10.0-4),
         x11-common,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: transfig (<< 1:3.2.6~beta-1~)
Breaks: transfig (<< 1:3.2.6~beta-1~)
Provides: transfig
Suggests: xfig
Multi-Arch: foreign
Description: Utilities for converting XFig figure files
 This package contains utilities (mainly fig2dev) to handle XFig
 (Facility for Interactive Generation of figures) files.
 .
 It can convert files produced by xfig to box, cgm, dxf, epic, eepic,
 eepicemu, emf, eps, gbx, ge, gif, ibmgl, jpeg, latex, map (HTML image
 map), mf (MetaFont), mp (MetaPost), mmp (Multi-Meta-Post), pcx, pdf,
 pdftex, pdftex_t, pic, pict2e, pictex, png, ppm, ps, pstex, pstex_t,
 pstricks, ptk (Perl/tk), shape, sld (AutoCad slide format), svg,
 textyl, tiff, tikz, tk (Tcl/Tk), tpic, xbm and xpm.
